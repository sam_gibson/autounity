from marshmallow import Schema, fields, EXCLUDE, post_load


def camelcase(s: str) -> str:
    """Convert a string to be in camelCase."""
    parts = iter(s.split("_"))
    return next(parts) + "".join(i.title() for i in parts)


class CamelCaseSchema(Schema):
    """Schema that uses camel-case for its external representation
    and snake-case for its internal representation.
    """
    def on_bind_field(self, field_name, field_obj):
        field_obj.data_key = camelcase(field_obj.data_key or field_name)


class UnityVersionSchema(CamelCaseSchema):
    """A marshmallow schema for a version of Unity, as in the Hub manifests.

    Fields:
        version (String): The version number
        download_url (Url): The URL of the file to download
        download_size (Integer): The size of the download
        checksum (String): The checksum of the file
    """
    version = fields.String(required=True)
    download_url = fields.Url(required=True)
    download_size = fields.Integer(required=True)
    checksum = fields.String(required=True)

    @post_load
    def make_unity_version(self, data, **kwargs):
        return UnityVersion(**data)


UNITY_VERSION_SCHEMA = UnityVersionSchema(many=True, unknown=EXCLUDE)
"""Instance of the version schema to use for validation."""


class UnityVersion():
    """A data container for information about versions of Unity.

    Attributes:
        version (str): The version number
        download_url (str): The URL of the file to download
        download_size (int): The size of the download
        checksum (str): The checksum of the file
    """
    def __init__(self, version: str, download_url: str, download_size: int,
                 checksum: str):
        self.version = version
        self.download_url = download_url
        self.download_size = download_size
        self.checksum = checksum

    def __str__(self):
        return f"{self.version}: {self.download_url}"
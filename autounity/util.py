"""util.py

Utility methods for autounity.

Author:
    Figglewatts <me@figglewatts.co.uk>
"""

from typing import List


def filter_dict(to_filter: dict, keys: List[str]) -> dict:
    """Filter the keys/values in a dict to a given subset.

    Args:
        to_filter (dict): The source dict to filter.
        keys (List[str]): The list of keys to filter from the dict.

    Returns:
        dict: The filtered dict.
    """
    return {key: to_filter[key] for key in keys}
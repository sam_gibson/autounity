import os

from appdirs import user_cache_dir

APP_NAME = "autounity"
APP_AUTHOR = "figglewatts"


def get_cache_dir() -> str:
    cache_dir = user_cache_dir(APP_NAME, APP_AUTHOR)
    if not os.path.exists(cache_dir):
        os.makedirs(cache_dir)
    return cache_dir
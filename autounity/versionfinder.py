import json
from typing import List, Union, Iterator
from os import path
import logging

import requests

from .cache import get_cache_dir
from .unity_version import UnityVersion, UNITY_VERSION_SCHEMA

UNITY_HUB_RELEASES_URL = """\
https://public-cdn.cloud.unity3d.com/hub/prod/releases-{0}.json"""

WINDOWS_PLATFORM_STRING = "win32"
OSX_PLATFORM_STRING = "darwin"
LINUX_PLATFORM_STRING = "linux"

VALID_PLATFORM_STRINGS = [
    WINDOWS_PLATFORM_STRING, OSX_PLATFORM_STRING, LINUX_PLATFORM_STRING
]


def __get_manifest_filepath(platform_str: str) -> str:
    # get the filename of the manifest from the releases URL
    cached_manifest_filename = path.basename(get_releases_url(platform_str))
    cached_manifest_path = path.join(get_cache_dir(), cached_manifest_filename)
    return cached_manifest_path


def __load_manifest(platform_str: str) -> List[UnityVersion]:
    # load the manifest from disk
    cached_manifest_path = __get_manifest_filepath(platform_str)
    with open(cached_manifest_path, "r") as manifest_file:
        loaded_manifest_raw = json.load(manifest_file)
        loaded_manifest = UNITY_VERSION_SCHEMA.load(loaded_manifest_raw)
        return loaded_manifest


def __dump_manifest(manifest: List[UnityVersion], platform_str: str):
    # serialize the manifest and dump it to disk
    cached_manifest_path = __get_manifest_filepath(platform_str)
    with open(cached_manifest_path, "w") as manifest_file:
        dumped_manifest = UNITY_VERSION_SCHEMA.dump(manifest)
        json.dump(dumped_manifest, manifest_file)


def fetch_hub_manifest(platform_str: str) -> List[UnityVersion]:
    url = get_releases_url(platform_str)
    manifest = requests.get(url, stream=True)
    if manifest.status_code == 200:
        loaded_manifest = UNITY_VERSION_SCHEMA.load(
            manifest.json()["official"])
        __dump_manifest(loaded_manifest, platform_str)
        return loaded_manifest
    else:
        logging.critical(
            f"Failed to get releases from '{url}', {manifest.status_code} response"
        )


def get_hub_manifest(platform_str: str) -> List[UnityVersion]:
    manifest_filepath = __get_manifest_filepath(platform_str)

    # check to see if we have it cached
    if path.exists(manifest_filepath):
        return __load_manifest(platform_str)

    # otherwise, fetch it
    return fetch_hub_manifest(platform_str)


def list_versions(platform_str: str) -> Iterator[str]:
    manifest = get_hub_manifest(platform_str)
    for m in manifest:
        yield m.version


def get_version(platform_str: str, version_number: str) -> UnityVersion:
    manifest = get_hub_manifest(platform_str)
    for m in manifest:
        if m.version == version_number:
            return m
    return None


def get_releases_url(platform_str: str) -> str:
    if platform_str not in VALID_PLATFORM_STRINGS:
        raise ValueError(f"platform_str {platform_str} is invalid")

    return UNITY_HUB_RELEASES_URL.format(platform_str)
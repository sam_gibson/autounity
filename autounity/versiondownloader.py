import os.path

import requests
from tqdm import tqdm

from .unity_version import UnityVersion


def download(version: UnityVersion):
    print(version.download_url)
    download_response = requests.get(version.download_url, stream=True)

    filename = os.path.basename(version.download_url)
    with open(filename, "wb") as download_file:
        for data in tqdm(download_response.iter_content(),
                         unit="b",
                         total=version.download_size,
                         unit_scale=True):
            download_file.write(data)
from setuptools import setup, find_packages


def readme():
    with open("README.md") as r:
        return r.read()


setup(name="autounity",
      version="0.1",
      description="Automation of various Unity tasks.",
      long_description=readme(),
      long_description_content_type="text/markdown",
      keywords="unity ci automation cd",
      classifiers=[
          "Development Status :: 3 - Alpha", "Intended Audience :: Developers",
          "License :: OSI Approved :: MIT License",
          "Programming Language :: Python :: 3 :: Only", "Topic :: Internet",
          "Topic :: Software Development :: Libraries :: Python Modules",
          "Environment :: Console", "Operating System :: Microsoft :: Windows",
          "Topic :: Games/Entertainment",
          "Topic :: Software Development :: Build Tools", "Topic :: Utilities"
      ],
      url="https://gitlab.com/sam_gibson/autounity",
      author="Figglewatts",
      author_email="me@figglewatts.co.uk",
      license="MIT",
      packages=find_packages(),
      zip_safe=False,
      include_package_data=True,
      scripts=["bin/autounity"])